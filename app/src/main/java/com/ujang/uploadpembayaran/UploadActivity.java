package com.ujang.uploadpembayaran;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ujang.uploadpembayaran.rest.ApiInterface;
import com.ujang.uploadpembayaran.rest.RestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadActivity extends AppCompatActivity {

    private TextView tvNik;
    String nik_pendaftar;
    private RelativeLayout frameLayout;
    private Button btnSubmit;
    private EditText et_keterangan;
    private Uri selectedImage;
    private String filePath = "";
    private ImageView image_upload;
    private Context mContext;
    private ProgressDialog loading;
    private static final int REQUEST_CAMERA = 1888;
    private static final int SELECT_FILE = 1887;
    private static final int PICK_FROM_GALLERY = 2;
    private Bundle inBundle;
    String nik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        setTitle("Posting Foto");
        mContext = this;
        inBundle = getIntent().getExtras();
        image_upload = (ImageView) findViewById(R.id.image_upload);
        frameLayout = (RelativeLayout) findViewById(R.id.frame_image);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseGallerOrCamera();
            }
        });

        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestUpload();
            }
        });

        nik = inBundle.get("nik").toString();
        tvNik = findViewById(R.id.tv_nik);
        tvNik.setText(nik);
    }

    private void ChooseGallerOrCamera() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    File photo = null;
                    try {
                        // place where to store camera taken picture
                        photo = this.createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {

                    }
                    selectedImage = Uri.fromFile(photo);
                    cameraIntent.putExtra("ket", "1");
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*"); //set type for files (image type)
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

            private File createTemporaryFile(String part, String ext) throws Exception {
                File tempDir = Environment.getExternalStorageDirectory();
                tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                }
                return File.createTempFile(part, ext, tempDir);
            }

        });
        builder.show();
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                ContentResolver cr = this.getContentResolver();
                Bitmap photo = null;
                try {
                    photo = MediaStore.Images.Media.getBitmap(cr, selectedImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImage = getImageUri(getApplicationContext(), photo);
                filePath = getRealPathFromURI(selectedImage);
                setImageView(filePath);

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(mContext, "Berhasil Mengambil Kamera", Toast.LENGTH_SHORT).show();

            } else {
                // failed to capture image

                Toast.makeText(mContext, "Maaf Gagal mengambil kamera", Toast.LENGTH_SHORT).show();

            }
        } else if (requestCode == PICK_FROM_GALLERY) {
            if (resultCode == RESULT_OK) {
                selectedImage = data.getData();
//                tampil_gambar_sk_hilang.setImageURI(selectedImage);
                filePath = getRealPathFromURIPath(selectedImage, this);
                setImageView(filePath);
            }
        }
    }


    private void setImageView(String filepath) {
        File imgFile = new File(filepath);
        if (imgFile.exists()) {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            File f = new File(filepath);
            Bitmap b = null;
            try {
                FileInputStream fis = new FileInputStream(f);
                BitmapFactory.decodeStream(fis, null, o);
                fis.close();

                float sc = 0.0f;
                int scale = 1;
                //if image height is greater than width
                if (o.outHeight > o.outWidth) {
                    sc = o.outHeight / 400;
                    scale = Math.round(sc);
                }
                //if image width is greater than height
                else {
                    sc = o.outWidth / 400;
                    scale = Math.round(sc);
                }

                // Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                fis = new FileInputStream(f);
                b = BitmapFactory.decodeStream(fis, null, o2);

                image_upload.setImageBitmap(b);
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void requestUpload() {
        nik = inBundle.get("nik").toString();
        loading = ProgressDialog.show(this, null, "Tunggu Sebentar...", true, false);
        File file = new File(filePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part evidanceFile = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
        Call<ResponseBody> insertImage = RestClient.getService().create(ApiInterface.class).upload(nik,evidanceFile);
        insertImage.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                if (response.isSuccessful()) {
                    try{
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());

                        if(jsonRESULTS.getString("status").equals("Created")){

                            Toast.makeText(mContext, "Berhasil di Upload", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(mContext, "Gagal", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Gagal Memuat Data", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
