package com.ujang.uploadpembayaran;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private EditText etNik;
    private Button btnCek;
    private String nik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etNik = findViewById(R.id.et_nik);
        btnCek = findViewById(R.id.btn_cek_pembayaran);

        btnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Upload2Activity.class);
                nik = etNik.getText().toString();
                i.putExtra("nik", nik);
                v.getContext().startActivity(i);
            }
        });
    }
}
