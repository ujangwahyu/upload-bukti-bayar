package com.ujang.uploadpembayaran.rest;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Ujang Wahyu on 12,November,2018
 */
public interface ApiInterface {
    @Multipart
    @PUT("pendaftar/{nik}/upload")
    Call<ResponseBody> upload(
            @Path("nik") String nik,
            @Part MultipartBody.Part bukti_bayar_pendaftar
    );
}
